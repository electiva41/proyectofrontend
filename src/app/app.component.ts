import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { PdfExportService } from './pdf-export.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private http: HttpClient, private pdfExportService: PdfExportService) { }
  
  columnas: string[] = ['cliente', 'total', 'factura', 'borrar', 'seleccionar'];
  columnasLibro: string[] = ['codigo', 'titulo', 'descripcion', 'autor','fecha_publicacion','borrar', 'seleccionar'];
  header: string[] = ['Cliente', 'Total', 'Factura'];
  headerLibros: string[] = ['Codigo', 'Titulo', 'Descripcion', 'Autor', 'Fecha'];
  newMenu: string = '';

  datos: any;
  datosLibros: any;

  ventaselect: Venta = new Venta(0, "", 0 , "");
  libroselect: Libro = new Libro(0,"","","","");

  @ViewChild(MatTable) tabla1!: MatTable<Venta>;
  @ViewChild(MatTable) tabla2!: MatTable<Venta>;

  borrarVenta(id: number) {
    if (confirm("¿Realmente desea eliminar esta venta?")) {
      this.http.delete(`http://localhost:9090/api/venta/${id}`)
          .subscribe(
              () => {
                  // Éxito: la venta se eliminó en el backend
                  // Puedes manejar aquí cualquier acción adicional que desees en el frontend
                  this.datos = this.datos.filter((venta: any) => venta.id !== id);
                  this.tabla1.renderRows();
              },
              (error) => {
                  // Manejo de errores en caso de que la solicitud al backend falle
                  console.error("Error al eliminar la venta:", error);
                  // Puedes mostrar un mensaje de error al usuario si es necesario
              }
          );
    }
  }

  borrarLibro(id: number) {
    if (confirm("¿Realmente desea eliminar este libro?")) {
      this.http.delete(`http://localhost:9090/api/libro/${id}`)
          .subscribe(
              () => {
                  // Éxito: la venta se eliminó en el backend
                  // Puedes manejar aquí cualquier acción adicional que desees en el frontend
                  this.datosLibros = this.datosLibros.filter((libro: any) => libro.id !== id);
                  this.tabla2.renderRows();
              },
              (error) => {
                  // Manejo de errores en caso de que la solicitud al backend falle
                  console.error("Error al eliminar el libro:", error);
                  // Puedes mostrar un mensaje de error al usuario si es necesario
              }
          );
    }
  }

  agregar() {
    // Datos a enviar
    const ventaData = {
      cliente: this.ventaselect.cliente,
      total: this.ventaselect.total,
      factura: this.ventaselect.factura
    };

    // Configura las cabeceras si es necesario
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    // Realiza la solicitud POST directa al backend
    this.http.post('http://localhost:9090/api/venta', ventaData, { headers })
      .subscribe(
        (data) => {
          // Éxito: la venta se agregó en el backend
          // Puedes manejar aquí cualquier acción adicional que desees en el frontend
          this.datos.push(data); // Agrega los datos a la tabla en el frontend
          this.tabla1.renderRows();
          this.ventaselect = new Venta(0, "", 0, "");
        },
        (error) => {
          // Manejo de errores en caso de que la solicitud al backend falle
          console.error("Error al agregar la venta:", error);
          // Puedes mostrar un mensaje de error al usuario si es necesario
        }
      );
  }

  agregarLibro() {
    // Datos a enviar
    const libroData = {
      codigo: this.libroselect.codigo,
      titulo: this.libroselect.titulo,
      descripcion: this.libroselect.descripcion,
      autor: this.libroselect.autor,
      fecha_publicacion: this.libroselect.fecha_publicacion
    };

    // Configura las cabeceras si es necesario
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    // Realiza la solicitud POST directa al backend
    this.http.post('http://localhost:9090/api/libro', libroData, { headers })
      .subscribe(
        (data) => {
          // Éxito: la venta se agregó en el backend
          // Puedes manejar aquí cualquier acción adicional que desees en el frontend
          this.datosLibros.push(data); // Agrega los datos a la tabla en el frontend
          this.tabla2.renderRows();
          this.libroselect = new Libro(0,"","","","");
        },
        (error) => {
          // Manejo de errores en caso de que la solicitud al backend falle
          console.error("Error al agregar el libro:", error);
          // Puedes mostrar un mensaje de error al usuario si es necesario
        }
      );
  }

  editarVenta(venta: Venta) {
    this.http.put<Venta>(`http://localhost:9090/api/venta/${venta.id}`, venta)
      .subscribe(
        (data) => {
          console.log("Edición exitosa:", data);
          // Éxito: la venta se actualizó en el backend
          // Puedes manejar aquí cualquier acción adicional que desees en el frontend
          // Por ejemplo, cerrar el formulario de edición
  
          // Encuentra el índice del elemento en this.datos
          const indice = this.datos.findIndex((item: Venta) => item.id === data.id);
  
          // Actualiza el elemento en this.datos
          this.datos[indice] = data;
  
          // Renderiza las filas de la tabla
          this.tabla1.renderRows();
        },
        (error) => {
          // Manejo de errores en caso de que la solicitud al backend falle
          console.error("Error al actualizar la venta:", error);
          // Puedes mostrar un mensaje de error al usuario si es necesario
        }
      );
  }

  editarLibro(libro: Libro) {
    this.http.put<Libro>(`http://localhost:9090/api/libro/${libro.id}`, libro)
      .subscribe(
        (data) => {
          console.log("Edición exitosa:", data);
          // Éxito: la venta se actualizó en el backend
          // Puedes manejar aquí cualquier acción adicional que desees en el frontend
          // Por ejemplo, cerrar el formulario de edición
  
          // Encuentra el índice del elemento en this.datos
          const indice = this.datosLibros.findIndex((item: Libro) => item.id === data.id);
  
          // Actualiza el elemento en this.datos
          this.datosLibros[indice] = data;
  
          // Renderiza las filas de la tabla
          this.tabla2.renderRows();
        },
        (error) => {
          // Manejo de errores en caso de que la solicitud al backend falle
          console.error("Error al actualizar el libro:", error);
          // Puedes mostrar un mensaje de error al usuario si es necesario
        }
      );
  }

  seleccionar(venta: Venta) {
    this.ventaselect.id = venta.id; // Asigna el valor del id
    this.ventaselect.cliente = venta.cliente;
    this.ventaselect.total = venta.total;
    this.ventaselect.factura = venta.factura;
  }

  seleccionarLibro(libro: Libro) {
    this.libroselect.id = libro.id; // Asigna el valor del id
    this.libroselect.codigo = libro.codigo;
    this.libroselect.titulo = libro.titulo;
    this.libroselect.descripcion = libro.descripcion;
    this.libroselect.autor = libro.autor;
    let fecha = new Date(libro.fecha_publicacion);
    this.libroselect.fecha_publicacion = this.setFecha(fecha);
  }

  setFecha(fecha:Date) {
    let dia = fecha.getDate();
    let diaS = dia < 10 ? '0'+dia: dia;
    let mes = fecha.getMonth()+1;
    let mesS = mes < 10 ? '0'+mes: mes;
    let anho = fecha.getFullYear();
    return `${anho}-${mesS}-${diaS}`
  }

  guardar() {
    console.log('ventaselect.id = ', this.ventaselect.id);
    if (this.ventaselect.id) {
      // Realizar lógica para actualizar el registro existente
      // Llama a la función para actualizar el registro
      this.editarVenta(this.ventaselect);
    } else {
      // Realizar lógica para agregar un nuevo registro
      // Llama a la función para agregar un nuevo registro
      this.agregar();
    }
  
    // Restablece el formulario después de la operación
    this.ventaselect = new Venta(0, "", 0, "");
  }  

  guardarLibro() {
    console.log('libroselect.id = ', this.libroselect.id);
    if (this.libroselect.id) {
      // Realizar lógica para actualizar el registro existente
      // Llama a la función para actualizar el registro
      this.editarLibro(this.libroselect);
    } else {
      // Realizar lógica para agregar un nuevo registro
      // Llama a la función para agregar un nuevo registro
      this.agregarLibro();
    }
  
    // Restablece el formulario después de la operación
    this.libroselect = new Libro(0,"","","","");
  }  

  ngOnInit() {
    this.http.get("http://localhost:9090/api/venta")
      .subscribe(
        resultado => {
          this.datos = resultado;
          if (!this.datos) this.datos = []
        }
      );

      this.http.get("http://localhost:9090/api/libro")
      .subscribe(
        resultado => {
          this.datosLibros = resultado;
          if (!this.datosLibros) this.datosLibros = []
        }
      );
  }

  exportToPdf(): void {
    //const headers = ['Name', 'Age', 'City'];
    const fileName = 'facturaciones';
    this.pdfExportService.exportToPdf(this.header, this.listadoPdf(this.datos), fileName);
  }

  exportToPdfLibro(): void {
    //const headers = ['Name', 'Age', 'City'];
    const fileName = 'libros';
    this.pdfExportService.exportToPdf(this.headerLibros, this.listadoPdfLibros(this.datosLibros), fileName);
  }

  listadoPdf(listadoForPdf:any[]):any {
    //let fec = new DatePipe('es_PY');
    //let mil = new DecimalPipe('es_PY');
    let newArray:any[] = [];
    let auxarray:any[] = [];
    if (listadoForPdf) {
      listadoForPdf.forEach((e:any)=> {
        auxarray = [];
        auxarray.push(e.cliente);
        auxarray.push(e.total);
        auxarray.push(e.factura);
        newArray.push(auxarray);
      })
      return newArray;
    } else {
      return null;
    }
  }

  listadoPdfLibros(listadoForPdf:any[]):any {
    //let fec = new DatePipe('es_PY');
    //let mil = new DecimalPipe('es_PY');
    let newArray:any[] = [];
    let auxarray:any[] = [];
    if (listadoForPdf) {
      listadoForPdf.forEach((e:any)=> {
        auxarray = [];
        auxarray.push(e.codigo);
        auxarray.push(e.titulo);
        auxarray.push(e.descripcion);
        auxarray.push(e.autor);
        auxarray.push(e.fecha_publicacion);
        newArray.push(auxarray);
      })
      return newArray;
    } else {
      return null;
    }
  }

  /*listadoPdf2(listadoForPdf:any[]): any {
    //let mil = new DecimalPipe('es_PY');
    let newArray:any[] = [];
    let auxarray:any[] = [];
    if (listadoForPdf) {
      listadoForPdf.forEach((e:any)=> {
        auxarray = [];
        auxarray.push(e.descBanco,);
        //auxarray.push(mil.transform(e.monto,'1.2-2'));
        auxarray.push(e.ruc);
        auxarray.push(e.razonSocial);
        auxarray.push(e.nroSolicitud);
        auxarray.push(e.nroBoleta);
        auxarray.push(e.idConfBanco);
        newArray.push(auxarray);
      })
      return newArray;
    } else {
      return null;
    } 
  }*/
  validarVista(menu:any): boolean {
    if (this.newMenu == '') return true;
    if (this.newMenu == menu) return true;
    return false;
  }
}


export class Venta {
  constructor(public id: number, public cliente: string, public total: number, public factura: string) {
  }
}

export class Libro {
  constructor(public id: number, public codigo: string, public titulo: string, public descripcion: string, public autor: string, public fecha_publicacion?: any){}
}