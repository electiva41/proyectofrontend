
import { Injectable } from '@angular/core';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
//npm install pdfmake --save
//npm install --save-dev @types/pdfmake

(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root',
})
export class PdfExportService {
  EXCEL_EXTENSION = '.pdf';
  constructor() {
  }
  exportToPdf<T>(headers1:string[], data1: T[], fileName: string): void {
    let bodies1 = [];
    bodies1.push(headers1)
    bodies1.push(...data1.map((row:any) => row));
    //bodies = data.map((row:any) => row);
    /*let bodies2 = [];
    bodies2.push(headers2)
    bodies2.push(...data2.map((row:any) => row));
   */
    const documentDefinition:any = {
      content: [
        //PRIMERA PARTE
        { text: '', style: 'header'},
        {
          width: 'auto',
          layout: 'lightHorizontalLines', // optional
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            //widths: [100, '*', 200, '*'],
            body: bodies1,
            alignment: "center"
          }
        },
        { width: '*', text: '' },
        // Salto de página
       /* { text: '', pageBreak: 'after' },
        //SEGUNDA PARTE
        { text: '2', style: 'header' },
        {
          layout: 'lightHorizontalLines', // optional
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            //widths: [100, '*', 200, '*'],
            body: bodies2
          }
        },*/
      ],
      styles: {
        header: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 2], // Ajusta los márgenes según tus necesidades
        },
      },
      defaultStyle: {
        // Define el estilo por defecto para el contenido
        fontSize: 12,
        margin: [0, 0, 0, 2], // Ajusta los márgenes según tus necesidades
      }
    };
   
    pdfMake.createPdf(documentDefinition).download(fileName + this.EXCEL_EXTENSION);
  }
}